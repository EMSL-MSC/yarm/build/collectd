[![pipeline status](https://gitlab.pnnl.gov/yarm/build/collectd/badges/master/pipeline.svg)](https://gitlab.pnnl.gov/yarm/build/collectd/commits/master)

```
docker build -t collectd-centos7 -f Dockerfile.centos7 .
```

```
docker run --rm=true -v `pwd`/output/:/output collectd-centos7
```
