#!/bin/bash -xe

./getgit.sh
cd collectd
yum install -y libgcrypt-devel glib2-devel libtool-ltdl-devel
./build.sh

yum-builddep -y contrib/redhat/collectd.spec

VER=`./version-gen.sh`
sed -i s/5.9.0/$VER/ contrib/redhat/collectd.spec
./configure
make dist
cp collectd-$VER.tar.bz2 /root/rpmbuild/SOURCES/

rpmbuild -bb contrib/redhat/collectd.spec --with mic --without lvm --without cuda

cp -rv /root/rpmbuild/RPMS/ /output
chmod -R 777 /output
